from django.db import models
from django.contrib.auth.models import User

class Tag(models.Model):
    user_id = models.ForeignKey(User)
    tag = models.CharField(max_length=50)
    def __unicode__(self):
        return u'%d' % (self.id)

class Customer(models.Model):
    name = models.CharField(max_length=50)
    twitter_name = models.CharField(max_length=50)
    def __unicode__(self):
        return u'%d' % (self.id)

class Tweet(models.Model):
    from_user = models.CharField(max_length=50)
    tweet = models.CharField(max_length=280)
    datetime = models.DateTimeField()
    img = models.CharField(max_length=280)
    def __unicode__(self):
        return u'%d' % (self.id)

