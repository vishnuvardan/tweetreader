def root_url(request):
    from django.conf import settings
    return {'root_url': settings.ROOT_URL}
