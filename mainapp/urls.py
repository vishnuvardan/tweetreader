from django.conf.urls.defaults import patterns, url, include
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('mainapp.views',
        url(r'^backup_tweets/$', 'backup_tweets'),
        url(r'^show_tweets/$', 'show_tweets'),
        url(r'^filter_tweets/$', 'filter_tweets'),
        url(r'^submit_login/$', 'submit_login'),
        url(r'^signout/', 'signout'),
        url(r'^signup/', 'createuser'),
        url(r'^addtag/', 'addtag'),
        url(r'^deletetag/', 'deletetag'),
        url(r'^$', direct_to_template, {'template': 'mainapp/templates/login.html'}),
    )

urlpatterns += patterns('',
        url(r'^static_media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': '/home/raghuveer/projects/tagTweetReader/mainapp/static_media/',
            'show_indexes' : True,
        }),
)

