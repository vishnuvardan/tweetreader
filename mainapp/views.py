# Create your views here.
import simplejson
import urllib2
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.exceptions import ObjectDoesNotExist
from django.template import loader, Context
from django.conf import settings
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.db import IntegrityError
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from datetime import datetime

from forms import *
from models import *


def backup(twitter_name):
    req = urllib2.Request(url = "https://api.twitter.com/1/statuses/user_timeline.json?include_entities=false&include_rts=false&screen_name="+ twitter_name +"&count=50&exclude_replies=false")
    urlrequest = urllib2.urlopen(req)
    resultString = urlrequest.read()
    json = simplejson.loads(resultString)
    for item in json:
        p = Tweet()
        p.from_user = item['user']['screen_name']
        p.img = item['user']['profile_image_url']
        dat = datetime.strptime(item['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
        p.datetime = dat
        p.tweet = item['text']
        p.save()
    return json

def backup_tweets(request):
    obj = Tweet.objects.all()
    obj.delete()
    jsonstring = []
    customers = Customer.objects.all()
    for customer in customers:
        jsonstring = jsonstring + backup(customer.twitter_name)
    return HttpResponse(jsonstring)

@login_required(login_url='/tweetreader/')
def show_tweets(request):
    obj = Tweet.objects.all().order_by('-datetime')
    paginator = Paginator(obj, 10).page(1)
    user = User.objects.get(id = request.user.id)
    tags = Tag.objects.filter(user_id = user)
    defaultTags = settings.DEFAULT_TAGS
    defaultTags = defaultTags.split(',')
    return render_to_response("mainapp/templates/index.html", {"tweets": paginator, "tags" : tags, "defaulttags" : defaultTags }, context_instance=RequestContext(request))

def filter_tweets(request):
    tags = request.GET['tags']
    page = request.GET['page']
    obj = []
    splitedTags = tags.split(',')
    for tag in splitedTags:
        obj += Tweet.objects.filter(tweet__icontains = tag).order_by('-datetime')
    paginator = Paginator(obj, 10).page(page)
    t = loader.get_template('mainapp/templates/tweets.html')
    c = Context({ 'tweets': paginator , 'root_url' : settings.ROOT_URL })
    rendered = t.render(c)
    return HttpResponse(rendered)

def submit_login(request):
    if request.method == 'POST': # If the form has been submitted...
        form = AuthenticationForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            usern = request.POST['username']
            passw = request.POST['password']
            user = authenticate(username=usern, password=passw)
            print user
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(settings.ROOT_URL+"tweetreader/show_tweets/")
                else:
                    return HttpResponseRedirect(settings.ROOT_URL+"tweetreader/")
            else:
                form.mismatch = 'The username and password did not match'
                return render_to_response('mainapp/templates/login.html', {
                    'form': form,
                }, context_instance=RequestContext(request))
    else:
        form = AuthenticationForm() # An unbound form

    return render_to_response('mainapp/templates/login.html', {
        'form': form,
    }, context_instance=RequestContext(request))

def signout(request):
    logout(request)
    return HttpResponseRedirect(settings.ROOT_URL+"tweetreader/")

def createuser(request):
    if request.method == 'POST': # If the form has been submitted...
        form = RegistrationForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            username = request.POST['rusername']
            email = request.POST['email']
            password = request.POST['rpassword']
            try:
                user = User.objects.create_user(username, email,password)
                user.save()
            except IntegrityError:
                form.exists = "Username is already exists."
                return render_to_response('mainapp/templates/login.html', {
                    'rform': form,
                }, context_instance=RequestContext(request))
            user = authenticate(username=username, password=password)
            login(request, user)
            return HttpResponseRedirect(settings.ROOT_URL+"tweetreader/show_tweets/")
    else:
        form = RegistrationForm() # An unbound form

    return render_to_response('mainapp/templates/login.html', {
        'rform': form,
    }, context_instance=RequestContext(request))

def addtag(request):
    tag = request.GET['tag']
    user = request.user
    try:
        gettag = Tag.objects.get(user_id = user, tag = tag)
        return HttpResponse("Existing")
    except ObjectDoesNotExist:
        newtag = Tag(user_id = user, tag = tag)
        newtag.save()
        return HttpResponse("Success")

def deletetag(request):
    tag = request.GET['tag']
    user = request.user
    try:
        gettag = Tag.objects.get(user_id = user, tag = tag)
        gettag.delete()
        return HttpResponse("Deleted")
    except ObjectDoesNotExist:
        return HttpResponse("Error")
