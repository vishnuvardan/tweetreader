from tagTweetReader.mainapp.models import *
from django.contrib import admin

class CustomersAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'twitter_name']
admin.site.register(Customer, CustomersAdmin)

class TagsAdmin(admin.ModelAdmin):
    list_display = ['id', 'user_id', 'tag']
admin.site.register(Tag, TagsAdmin)
