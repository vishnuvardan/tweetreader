from django import forms

class RegistrationForm(forms.Form):
    rusername = forms.CharField(max_length=30, required=True)
    rpassword = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(required=True)

class AuthenticationForm(forms.Form):
    username = forms.CharField(max_length=30, required=True)
    password = forms.CharField(max_length=30, required=True)
