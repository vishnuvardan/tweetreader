	
$(document).ready(function(){
	$('.filter input').live('change', function(){
		tags = ""
		
		$('.filter input').each(function(){
			if ($(this).attr('checked') == 'checked'){
				tags += $(this).attr('value') + ","	
				}
		});
		tags = tags.substring(0, tags.length-1);
		$.ajax({
			url: "../filter_tweets/?tags="+tags+"&page=1",
			success: function(data){
				$('.tweets').empty();
				$('.tweets').html(data);
			}
		});     
		return false;
	});
	
	$('.next').live('click', function(){
		tags = ""
		
		$('.filter input').each(function(){
			if ($(this).attr('checked') == 'checked'){
				tags += $(this).attr('value') + ","	
				}
		});
		tags = tags.substring(0, tags.length-1);
		page = parseInt($('#pagenumber').text())+1;
		$.ajax({
			url: "../filter_tweets/?tags="+tags+"&page="+page,
			success: function(data){
				$('.tweets').empty();
				$('.tweets').html(data);
			}
		});     
		return false;
	});
	
	$('.prev').live('click', function(){
		tags = ""
		
		$('.filter input').each(function(){
			if ($(this).attr('checked') == 'checked'){
				tags += $(this).attr('value') + ","	
				}
		});
		tags = tags.substring(0, tags.length-1);
		page = parseInt($('#pagenumber').text())-1;
		$.ajax({
			url: "../filter_tweets/?tags="+tags+"&page="+page,
			success: function(data){
				$('.tweets').empty();
				$('.tweets').html(data);
			}
		});     
		return false;
	});

	$('#addtag').click(function(){
		tag = $('#newtag').val();
		$.ajax({
			url: "../addtag/?tag="+tag,
			success: function(data){
				if (data == "Success"){
					$('.userlevel').append('<span class="box"><input type = "checkbox" value = "'+tag+'">'+tag+'</input> &nbsp;<a href="#" id="removetag">X</a></span>');			
				}
				else{
					alert("This is already added.");
				}
			}
		});     
		return false;

	});

	$('.taglist span a').live('click', function(){
		var tag = $(this).parent().find('input').val();
		var thisobj = $(this);
		$.ajax({
			url: "../deletetag/?tag="+tag,
			success: function(data){
				if (data == "Deleted"){
					thisobj.parent().remove();	
				}
				else{
					alert("Error, please try again.");
				}
			}
		});     
		return false;
		
	});
});
